import steam as stm
import numpy as np

# Model PZR as a cylinder
# Geometry
D =  40.0 # inches
H = 200.0 # inches
# ICs
P = 2200.0 # psia
L = 100.0  # inches

A = np.pi*0.25*D*D
V = A*H

Vsi = V*(0.0254**3) # volume in m3
Asi = A*(0.0254**2) # area in m2
Hsi = H*0.0254      # height in m

psi2MPa = 0.00689475729 


Psi = P*psi2MPa
Tsi = stm.tsatp(Psi)

# Calculate Initial Conditions
Lsi = L*0.0254  # level in meters
Vfsi = Lsi*Asi
Vgsi = (Hsi-Lsi)*Asi
vf   = stm.vfsatp(Psi)
vg   = stm.vgsatp(Psi)
hf   = stm.hfsatp(Psi)
hg   = stm.hgsatp(Psi)
mf    = Vfsi/vf
mg    = Vgsi/vg
m     = mf + mg
v     = Vsi/m
x     = mg/m
h     = hf + x*(hg-hf)
H     = m*h
Hcheck = mf*hf + mg*hg

T    = 1.8*Tsi - 459.67

print  '|{0:8s}|{1:10s}|{2:12s}|{3:12s}|{4:8s}|'.format('p,psia','T,F','x','H,kJ','L,in') 
print  '|{0:8.1f}|{1:10.4f}|{2:12.4e}|{3:12.1f}|{4:8.2f}|'.format(P,T,x,H,L) 

pvec = [2000.0,1800.0,1600.0,1400.0,1200.0,1000.0,800.0,600.0,400.0,200.0,100.0,50.0,20.0,14.7,5.0,1.0,0.5]

for Pt in pvec:
	Psi = Pt*psi2MPa
	Tsi = stm.tsatp(Psi)
	vf   = stm.vfsatp(Psi)
	vg   = stm.vgsatp(Psi)
	hf   = stm.hfsatp(Psi)
	hg   = stm.hgsatp(Psi)	
	x    = (v - vf)/(vg-vf)
	mg   = x*m
	mf   = m - mg
	Vfsi = vf*mf
	Lsi  = Vfsi/Asi

	T    = 1.8*Tsi - 459.67
	H    = mf*hf + mg*hg
	L    = Lsi/0.0254
	print  '|{0:8.1f}|{1:10.4f}|{2:12.4e}|{3:12.1f}|{4:8.2f}|'.format(Pt,T,x,H,L) 
